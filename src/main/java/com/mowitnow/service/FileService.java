package com.mowitnow.service;

import static java.lang.Integer.parseInt;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.mowitnow.domain.Lawn;
import com.mowitnow.domain.Mower;
import com.mowitnow.enums.Command;
import com.mowitnow.exception.MowException;
import com.mowitnow.utils.MowerConfig;

/**
 * 
 * class to control data comming from files
 *
 */
public class FileService {

	/**
	 * loadData to load data from file in resource repository
	 * 
	 * @param fileName
	 * @return
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	public String loadData(String fileName) throws IOException, URISyntaxException {
		Path path = Paths.get(FileService.class.getClassLoader().getResource(fileName).toURI());
		List<String> lines = Files.lines(path, StandardCharsets.UTF_8).collect(Collectors.toList());
		StringBuilder sb = new StringBuilder("");
		for (int i = 0; i < lines.size(); i++) {
			sb.append(lines.get(i));
			if (i < lines.size() - 1) {
				sb.append("\n");
			}
		}
		return sb.toString();
	}

	/**
	 * checkFile to check that the file respects the following rules: number of odd
	 * lines, the first line respects the rule (The first line corresponds to the
	 * coordinates of the upper right corner of the lawn, those of the lower left
	 * corner are assumed to be (0,0))
	 * 
	 * @param filePath
	 * @return Lawn
	 * @throws IOException
	 * @throws MowException
	 * @throws URISyntaxException
	 */
	public Lawn checkFile(String filePath) throws IOException, MowException, URISyntaxException {
		String fileContent = loadData(filePath);
		String[] lines = fileContent.split("\n");
		if (((lines.length % 2) == 1)) {
			if (lines[0].matches("[0-9]* [0-9]*")) {
				String[] xy = lines[0].split(" ");
				int width = parseInt(xy[0]);
				int height = parseInt(xy[1]);
				int nbMower = (lines.length - 1) / 2;
				List<Mower> mowers = new ArrayList<Mower>();
				for (int i = 0; i < nbMower; i++) {
					mowers.add(configureMower(lines[(2 * i) + 1], lines[(2 * i) + 2]));
				}
				return new Lawn(width, height, mowers);
			} else {
				throw new MowException(
						"la dimension de la pelouse ne doit contenir que les valeurs de largeur et de hauteur");
			}
		} else {
			throw new MowException(
					"la configuration de la pelouse doit contenir une ligne pour les dimensions de la pelouse plus 2 lignes par tondeuse");
		}
	}

	/**
	 * configureMower to check initial parameter of the mower(initial position,
	 * commands) and configure the mower
	 * 
	 * @param position
	 * @param commandChars
	 * @param lawnWidth
	 * @param lawnHeight
	 * @return
	 * @throws MowException
	 */
	public static Mower configureMower(String position, String commandChars) throws MowException {
		if (position.matches("[0-9]* [0-9]* [NEWS]")) {
			if (commandChars.matches("[GDA]*")) {
				String[] config = position.split(" ");
				List<Command> commands = new ArrayList<Command>();
				for (char c : commandChars.toCharArray()) {
					commands.add(MowerConfig.COMMANDS_BY_NAME.get(c));
				}
				return new Mower(parseInt(config[0]), parseInt(config[1]),
						MowerConfig.DIRECTIONS_BY_NAME.get(config[2]), commands);
			} else {
				throw new MowException("Les commandes doivent �tre l'une des suivantes: G, D, A");
			}
		} else {
			throw new MowException("la position de la tondeuse doit �tre de la forme X Y Direction");
		}

	}
}
