package com.mowitnow.service;

import com.mowitnow.domain.Lawn;
import com.mowitnow.domain.Mower;
import com.mowitnow.enums.Command;
import com.mowitnow.enums.Direction;
import com.mowitnow.utils.MowerConfig;

/**
 * class to control mowers
 */
public class MowerService {
	/**
	 * launchMowers allows launching all mowers
	 * 
	 * @param mowers
	 */
	public void launchMowers(Lawn lawn) {
		for (Mower mower : lawn.getMowers()) {
			launchMower(mower, lawn.getWidth(), lawn.getHeight());
			System.out.println(mower.getX() + " " + mower.getY() + " " + mower.getDirection().name().charAt(0));
		}
	}

	/**
	 * launch Mower: launch the tasks of a mower
	 * 
	 * @param mower
	 * @param lawnWidth
	 * @param lawnHeight
	 */
	public void launchMower(Mower mower, int lawnWidth, int lawnHeight) {
		while (!mower.getCommands().isEmpty()) {
			executeStep(mower, lawnWidth, lawnHeight);
		}
	}

	/**
	 * executeStep allows the execution of an instruction
	 * 
	 * @param mower
	 * @param lawnWidth
	 * @param lawnHeight
	 */
	public void executeStep(Mower mower, int lawnWidth, int lawnHeight) {
		if (!mower.getCommands().isEmpty()) {
			Command command = mower.getCommands().get(0);
			if (Command.ADVANCE.equals(command)) {
				moveMower(mower, lawnWidth, lawnHeight);
			} else if (Command.RIGHT.equals(command) || Command.LEFT.equals(command)) {
				mower.setDirection(defineNewDirection(mower.getDirection(), command));
			}
			mower.getCommands().remove(0);
		}

	}

	/**
	 * defineNewDirection to define the new direction
	 * 
	 * @param direction
	 * @param side
	 * @return new direction
	 */
	Direction defineNewDirection(Direction direction, Command side) {
		if (Command.LEFT.equals(side)) {
			return MowerConfig.GO_LEFT.get(direction);
		} else {
			return MowerConfig.GO_RIGHT.get(direction);
		}

	}

	/**
	 * moveMower allows the mower to move forward on the X or Y axis of the lawn
	 * 
	 * @param mower
	 * @param lawnWidth
	 * @param lawnHeight
	 */
	private void moveMower(Mower mower, int lawnWidth, int lawnHeight) {
		switch (mower.getDirection()) {
		case NORTH:
			if (mower.getY() < lawnHeight) {
				mower.setY(mower.getY() + 1);
			}
			break;
		case EAST:
			if (mower.getX() < lawnWidth) {
				mower.setX(mower.getX() + 1);
			}
			break;
		case SOUTH:
			if (mower.getY() > 0) {
				mower.setY(mower.getY() - 1);
			}
			break;
		case WEST:
			if (mower.getX() > 0) {
				mower.setX(mower.getX() - 1);
			}
			break;
		}
	}
}
