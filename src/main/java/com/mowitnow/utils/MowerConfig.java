package com.mowitnow.utils;

import static com.mowitnow.enums.Command.ADVANCE;
import static com.mowitnow.enums.Command.LEFT;
import static com.mowitnow.enums.Command.RIGHT;
import static com.mowitnow.enums.Direction.EAST;
import static com.mowitnow.enums.Direction.NORTH;
import static com.mowitnow.enums.Direction.SOUTH;
import static com.mowitnow.enums.Direction.WEST;

import java.util.Map;

import com.mowitnow.enums.Command;
import com.mowitnow.enums.Direction;

/**
 * class to define the different directions and commands
 */ 
public class MowerConfig {
	public static final Map<String, Direction> DIRECTIONS_BY_NAME = Map.of("N", NORTH, "S", SOUTH, "E", EAST, "W",
			WEST);
	public static final Map<Character, Command> COMMANDS_BY_NAME = Map.of('A', ADVANCE, 'G', LEFT, 'D', RIGHT);
	public static final Map<Direction, Direction> GO_RIGHT = Map.of(NORTH, EAST, EAST, SOUTH, SOUTH, WEST, WEST, NORTH);
	public static final Map<Direction, Direction> GO_LEFT = Map.of(NORTH, WEST, WEST, SOUTH, SOUTH, EAST, EAST, NORTH);

}
