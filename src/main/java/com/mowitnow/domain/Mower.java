package com.mowitnow.domain;

import java.util.List;

import com.mowitnow.enums.Command;
import com.mowitnow.enums.Direction;

/**
 * class to define Mower
 */
public class Mower {

	private int x;
	private int y;
	private Direction direction;
	private final List<Command> commands;

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public List<Command> getCommands() {
		return commands;
	}


	public Mower(int x, int y, Direction direction, List<Command> commands) {
		this.x = x;
		this.y = y;
		this.direction = direction;
		this.commands = commands;
	}

}
