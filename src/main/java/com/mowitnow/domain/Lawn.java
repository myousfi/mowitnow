package com.mowitnow.domain;

import java.util.List;

/**
 * class to define Lawn
 */
public class Lawn {
	private final int height;
	private final int width;
	private final List<Mower> mowers;

	public Lawn(int width, int height, List<Mower> mowers) {
		this.height = height;
		this.width = width;
		this.mowers = mowers;
	}

	public int getHeight() {
		return this.height;
	}

	public int getWidth() {
		return this.width;
	}

	public List<Mower> getMowers() {
		return this.mowers;
	}
}
