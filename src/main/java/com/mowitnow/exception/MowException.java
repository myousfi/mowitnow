package com.mowitnow.exception;
/**
 * 
 * class to define custom exception
 *
 */
public class MowException extends Exception{
	public MowException(String message)
	  {
	    super(message);
	  }

}
