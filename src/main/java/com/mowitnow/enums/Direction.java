package com.mowitnow.enums;

/**
 * enum to define directions
 * 
 */
public enum Direction {
	NORTH, EAST, SOUTH, WEST;
}
