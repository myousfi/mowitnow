package com.mowitnow.enums;

/**
 * enum to control the mower
 */
public enum Command {
	ADVANCE, RIGHT, LEFT;
}
