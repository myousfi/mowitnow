package com.mowitnow;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.Test;

import com.mowitnow.domain.Lawn;
import com.mowitnow.enums.Direction;
import com.mowitnow.exception.MowException;
import com.mowitnow.service.FileService;
import com.mowitnow.service.MowerService;

/**
 * 
 * Class to test Mower Service
 *
 */
public class MowerServiceTest {

	@Test
	public void launchMowersWhenIsOk() throws IOException, MowException, URISyntaxException {
		Lawn lawn = new FileService().checkFile("testMawerData.txt");
		new MowerService().launchMowers(lawn);

		assertEquals(lawn.getMowers().size(), 2);

		assertEquals(lawn.getMowers().get(0).getX(), 1);
		assertEquals(lawn.getMowers().get(0).getY(), 3);
		assertEquals(lawn.getMowers().get(0).getDirection(), Direction.NORTH);

		assertEquals(lawn.getMowers().get(1).getX(), 5);
		assertEquals(lawn.getMowers().get(1).getY(), 1);
		assertEquals(lawn.getMowers().get(1).getDirection(), Direction.EAST);

	}

	@Test(expected = MowException.class)
	public void launchMowersWhenLinesNumberIsKo() throws IOException, MowException, URISyntaxException {
		Lawn lawn = new FileService().checkFile("testMawerDataWhenLineNumberIsKo.txt");
		new MowerService().launchMowers(lawn);
	}

	@Test(expected = MowException.class)
	public void launchMowersWhenLawnSizeIsKo() throws IOException, MowException, URISyntaxException {
		Lawn lawn = new FileService().checkFile("testMawerDataWhenLawnSizeIsKo.txt");
		new MowerService().launchMowers(lawn);
	}

	@Test(expected = MowException.class)
	public void launchMowersWhenDirectionIsKo() throws IOException, MowException, URISyntaxException {
		Lawn lawn = new FileService().checkFile("testMawerDataWhenDirectionIsKo.txt");
		new MowerService().launchMowers(lawn);
	}

	@Test(expected = MowException.class)
	public void launchMowersWhenInitialPositionIsKo() throws IOException, MowException, URISyntaxException {
		Lawn lawn = new FileService().checkFile("testMawerDataWhenInitialPositionIsKo.txt");
		new MowerService().launchMowers(lawn);
	}

}
